""" Parts of the U-Net model """
"""This code is modified from https://github.com/milesial/Pytorch-UNet"""

import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict


class DoubleConv(nn.Module):
    """(convolution => [BN] => ReLU) * 2"""

    def __init__(self, in_channels, out_channels, mid_channels=None, is_dropout=True, is_IN=True):
        super().__init__()
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = nn.ModuleList([])
        # self.double_conv = nn.Sequential(
        #     nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1),
        #     nn.InstanceNorm2d(mid_channels),
        #     nn.ReLU(inplace=True),
        #     nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1),
        #     nn.InstanceNorm2d(out_channels),
        #     nn.ReLU(inplace=True)
        # )

        #first conv
        self.double_conv.append(nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1))
        # if is_dropout:
        #     self.double_conv.append(nn.Dropout(0.2))
        if is_IN:
            self.double_conv.append(nn.InstanceNorm2d(mid_channels))
        else:
            self.double_conv.append(nn.BatchNorm2d(mid_channels))
        self.double_conv.append(nn.ReLU(inplace=True))

        #second conv
        self.double_conv.append(nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1))
        # if is_dropout:
        #     self.double_conv.append(nn.Dropout(0.2))
        if is_IN:
            self.double_conv.append(nn.InstanceNorm2d(out_channels))
        else:
            self.double_conv.append(nn.BatchNorm2d(out_channels))
        self.double_conv.append(nn.ReLU(inplace=True))


    def forward(self, x):
        for l in self.double_conv:
            x = l(x)
        return x


class Down(nn.Module):
    """Downscaling with maxpool then double conv"""

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            DoubleConv(in_channels, out_channels, is_IN = True)
        )

    def forward(self, x):
        return self.maxpool_conv(x)


class Up(nn.Module):
    """Upscaling then double conv"""

    def __init__(self, in_channels, out_channels, bilinear=True):
        super().__init__()

        # if bilinear, use the normal convolutions to reduce the number of channels
        #TODO: Consider add instance norm in DoubleConv
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = DoubleConv(in_channels, out_channels, in_channels // 2)
        else:
            self.up = nn.ConvTranspose2d(in_channels , in_channels // 2, kernel_size=2, stride=2)
            self.conv = DoubleConv(in_channels, out_channels)


    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is CHW
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        # if you have padding issues, see
        # https://github.com/HaiyongJiang/U-Net-Pytorch-Unstructured-Buggy/commit/0e854509c2cea854e247a9c615f175f76fbb2e3a
        # https://github.com/xiaopeng-liao/Pytorch-UNet/commit/8ebac70e633bac59fc22bb5195e513d5832fb3bd
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)

class QualityEstimation(nn.Module):
    def __init__(self):
        super(QualityEstimation, self).__init__()
        self.estimator1 = DoubleConv(1024, 256, mid_channels=512, is_IN = False)
        self.estimator2 = DoubleConv(256, 64, mid_channels=128, is_IN = False)
        self.estimator3 = DoubleConv(64, 16, mid_channels=32, is_IN = False)
        self.conv = nn.Conv2d(16, 1, kernel_size=3, padding=1)
        self.pooling = torch.nn.AdaptiveAvgPool2d(1)
        self.activation = nn.Sigmoid()

    def forward(self, x):
        x = self.estimator1(x)
        x = self.estimator2(x)
        x = self.estimator3(x)
        x = self.conv(x)
        x = self.pooling(x)
        # x = self.activation(x)

        return x
import argparse
import os
import numpy as np
import math
import itertools
import random

import torchvision.transforms as transforms
from torchvision.utils import save_image

from torch.utils.data import DataLoader

from torch.optim import lr_scheduler
import torch.nn as nn
import torch.nn.functional as F
import torch

from Unet import UNet
from discriminator import discriminator
from dataset import ImageDataset
import kornia

parser = argparse.ArgumentParser()
parser.add_argument("--n_epochs", type=int, default=550, help="number of epochs of training")
parser.add_argument("--epoch", type=int, default=0, help="number of epochs start training")
parser.add_argument("--decay_epoch", type=int, default=50, help="number of epochs start decaying LR")
parser.add_argument("--batch_size", type=int, default=22, help="size of the batches")
parser.add_argument("--lr", type=float, default=1e-3, help="learning rate")
parser.add_argument("--lambda_similarity", type=float, default=0.008, help="similarity loss weight")
parser.add_argument("--lambda_mask", type=float, default=6.9999e-3, help="mask loss weight")
parser.add_argument("--lambda_g_adv", type=float, default=0.985, help="adversarial g adv loss")
parser.add_argument("--lambda_d_adv", type=float, default=0.985, help="adversarial d adv loss")
parser.add_argument("--lambda_smooth", type=float, default=0.8, help="mask smooth loss")
parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
parser.add_argument("--dataroot", type=str, default='datasets/bad2good/', help='root directory of the dataset')
parser.add_argument("--width", type=int, default=640, help='target image width')
parser.add_argument("--height", type=int, default=200, help='target image height')
parser.add_argument("--save_name", type=str, default='test', help='name of the model')
parser.add_argument("--beta1", type=float, default=0.9, help='beta1 for adam')
parser.add_argument("--beta2", type=float, default=0.999, help='beta2 for adam')
parser.add_argument("--hcut", type=float, default=0.45, help='Horizontal cutting portion')
parser.add_argument("--n_critic", type=int, default=1, help="number of training steps for discriminator per iter")
parser.add_argument("--good_penalty", type=int, default=100.0, help="good mask penalty term")
parser.add_argument('--NoHSV', default=False, action='store_true', help='if HSV stacked')

opt = parser.parse_args()
print(opt)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class LambdaLR():
    def __init__(self, n_epochs, offset, decay_start_epoch):
        '''
        n_epochs: number of total training epochs
        offset: train start epochs
        decay_start_epoch: epoch start decay
        '''
        assert ((n_epochs - decay_start_epoch) > 0), "Decay must start before the training session ends!"
        self.n_epochs = n_epochs
        self.offset = offset
        self.decay_start_epoch = decay_start_epoch

    def step(self, epoch):
        return 1.0 - max(0, epoch + self.offset - self.decay_start_epoch)/(self.n_epochs - self.decay_start_epoch)

def save_model(name, generator, epoch):
    torch.save(generator.state_dict(), os.path.join(name, f'netG_{epoch}.pth'))



def get_target_tensor(prediction, target_is_real):
        """Create label tensors with the same size as the input.
        Parameters:
        """
        if target_is_real:
            target_tensor =  torch.tensor(1.0) # real label
        else:
            target_tensor = torch.tensor(0.0) # fake label
        return target_tensor.expand_as(prediction)

def smooth_loss(mat):
    '''
    compute smooth loss for mask.
    '''
    return torch.sum(torch.abs(mat[:, :, :, :-1] - mat[:, :, :, 1:])) + \
            torch.sum(torch.abs(mat[:, :, :-1, :] - mat[:, :, 1:, :]))

def resize_tensor(tensor,ratio):
    return F.interpolate(tensor,(int(opt.height/ratio), int(opt.width/ratio)), mode="bicubic")


# Loss function
adversarial_loss_3 = nn.BCEWithLogitsLoss()
adversarial_loss_4 = nn.BCEWithLogitsLoss()
adversarial_loss_5 = nn.BCEWithLogitsLoss()
adversarial_loss_6 = nn.BCEWithLogitsLoss() #enlarge
similarity_loss = nn.SmoothL1Loss()

# Initialize generator and discriminator
num_channel = 6
if opt.NoHSV:
    num_channel = 3
print(f"Number of input channels:{num_channel}")
generator = UNet(num_channel, bilinear=True)
discriminator_3 = discriminator(3,64,6)
discriminator_4 = discriminator(3,64,6)
discriminator_5 = discriminator(3,64,6)
discriminator_6 = discriminator(3,64,6)

generator = nn.DataParallel(generator)
discriminator_3 = nn.DataParallel(discriminator_3)
discriminator_4 = nn.DataParallel(discriminator_4)
discriminator_5 = nn.DataParallel(discriminator_5)
discriminator_6 = nn.DataParallel(discriminator_6)

generator.to(device)
discriminator_3.to(device)
discriminator_4.to(device)
discriminator_5.to(device)
discriminator_6.to(device)
adversarial_loss_3.to(device)
adversarial_loss_4.to(device)
adversarial_loss_5.to(device)
adversarial_loss_6.to(device)
similarity_loss.to(device)

# Configure data loader
transforms_ = [ transforms.RandomHorizontalFlip(),
                transforms.Lambda(lambda img: img.crop((0, int(img.size[1] * (opt.hcut + ((random.random() - 0.5) / 10.0))), img.size[0], img.size[1]))),
                transforms.Resize((opt.height + 50, opt.width + 150)),
                transforms.RandomCrop((opt.height, opt.width)),
                transforms.RandomAffine(degrees=10, translate=(0.1, 0.1), scale=(0.9, 1), shear=(6, 6)),
                transforms.ColorJitter(0.3, 0.3, 0.3),
                transforms.ToTensor()
                ]

dataloader = DataLoader(ImageDataset(opt.dataroot, transforms_=transforms_, unaligned=True), 
                        batch_size=opt.batch_size, shuffle=True, num_workers=opt.n_cpu)

# Optimizers
optimizer_G = torch.optim.AdamW(generator.parameters(), lr=opt.lr, betas=(opt.beta1, opt.beta2), weight_decay=1e-2)
optimizer_D = torch.optim.AdamW(itertools.chain(discriminator_3.parameters(), discriminator_4.parameters(), discriminator_5.parameters(), discriminator_6.parameters()), lr=opt.lr, betas=(opt.beta1, opt.beta2), weight_decay=1e-2)

#learning rate scheduler
lr_scheduler_G = torch.optim.lr_scheduler.LambdaLR(optimizer_G, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)
lr_scheduler_D = torch.optim.lr_scheduler.LambdaLR(optimizer_D, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)


#check saving path exists before training
save_path='%s/%s' % (opt.save_name, 'training')
save_image_path = os.path.join(save_path, 'training_samples')
if not os.path.exists(save_path):
    os.makedirs(save_path)
if not os.path.exists(save_image_path):
    os.makedirs(save_image_path)

# ----------
#  Training
# ----------


for epoch in range(opt.n_epochs):
    for i, batch in enumerate(dataloader):
        torch.cuda.empty_cache()
        # Set model input
        real_A = batch['A'].to(device)
        real_B = batch['B'].to(device)

        if opt.NoHSV:
            real_A_stacked = real_A
            real_B_stacked = real_B
        else:
            real_A_HSV = kornia.color.rgb_to_hsv(real_A)
            real_B_HSV = kornia.color.rgb_to_hsv(real_B)
            real_A_stacked = torch.cat((real_A, real_A_HSV),dim=1)
            real_B_stacked = torch.cat((real_B, real_B_HSV),dim=1)


        # -----------------
        #  Train Generator
        # -----------------

        optimizer_G.zero_grad()

        # Generate good and bad quality, mask and content
        bad_mask, bad_content = generator(real_A_stacked)
        good_mask, good_content = generator(real_B_stacked)

        reconstructed_bad_rgb = (1.0 - bad_mask) * real_A[:,0:3] + bad_mask * bad_content
        reconstructed_good_rgb = (1.0 - good_mask) * real_B[:,0:3] + good_mask * good_content

        d_bad_3 = discriminator_3(reconstructed_bad_rgb)
        label_bad_3 = get_target_tensor(d_bad_3, True).to(device)
        g_adversarial_loss_3 = adversarial_loss_3(d_bad_3, label_bad_3)

        d_bad_4 = discriminator_4(resize_tensor(reconstructed_bad_rgb, 2.0))
        label_bad_4 = get_target_tensor(d_bad_4, True).to(device)
        g_adversarial_loss_4 = adversarial_loss_4(d_bad_4, label_bad_4)

        d_bad_5 = discriminator_5(resize_tensor(reconstructed_bad_rgb, 4.0))
        label_bad_5 = get_target_tensor(d_bad_5, True).to(device)
        g_adversarial_loss_5 = adversarial_loss_5(d_bad_5, label_bad_5)

        d_bad_6 = discriminator_6(resize_tensor(reconstructed_bad_rgb, 0.5))
        label_bad_6 = get_target_tensor(d_bad_6, True).to(device)
        g_adversarial_loss_6 = adversarial_loss_6(d_bad_6, label_bad_6)

        #calculate loss for generator
        color_smooth_loss = smooth_loss(bad_content)
        mask_smooth_loss = smooth_loss(bad_mask)
        mask_norm_loss = torch.norm(bad_mask, 2) + torch.norm(good_mask, 2) * opt.good_penalty
        total_similarity_loss = similarity_loss(real_A, reconstructed_bad_rgb)
        g_adversarial_loss = g_adversarial_loss_3 + \
                            g_adversarial_loss_4 + \
                            g_adversarial_loss_5 + \
                            g_adversarial_loss_6

        g_loss = opt.lambda_g_adv * g_adversarial_loss + \
                opt.lambda_similarity * total_similarity_loss + \
                opt.lambda_mask * mask_norm_loss + \
                (1.0 - opt.lambda_g_adv - opt.lambda_similarity - opt.lambda_mask) * \
                (opt.lambda_smooth * mask_smooth_loss + (1-opt.lambda_smooth) * color_smooth_loss)


        g_loss.backward()
        optimizer_G.step()

        critic_number = opt.n_critic

        if i % critic_number == 0:
            # ---------------------
            #  Train Discriminator
            # ---------------------
            reconstructed_bad_rgb = reconstructed_bad_rgb.detach()
            real_B_rgb = real_B

            optimizer_D.zero_grad()

            # generate discriminator result
            dis_bad_3 = discriminator_3(reconstructed_bad_rgb)
            dis_real_3 = discriminator_3(real_B_rgb)

            dis_bad_label_3 = get_target_tensor(dis_bad_3, False).to(device)
            dis_real_label_3 = get_target_tensor(dis_real_3, True).to(device)
            # calculate d loss
            d_loss_3 = (adversarial_loss_3(dis_bad_3, dis_bad_label_3) + adversarial_loss_3(dis_real_3, dis_real_label_3)) * opt.lambda_d_adv

            # generate discriminator result
            dis_bad_4 = discriminator_4(resize_tensor(reconstructed_bad_rgb, 2.0))
            dis_real_4 = discriminator_4(resize_tensor(real_B_rgb, 2.0))

            dis_bad_label_4 = get_target_tensor(dis_bad_4, False).to(device)
            dis_real_label_4 = get_target_tensor(dis_real_4, True).to(device)
            d_loss_4 = (adversarial_loss_4(dis_bad_4, dis_bad_label_4) + adversarial_loss_4(dis_real_4, dis_real_label_4)) * opt.lambda_d_adv

            dis_bad_5 = discriminator_5(resize_tensor(reconstructed_bad_rgb, 4.0))
            dis_real_5 = discriminator_5(resize_tensor(real_B_rgb, 4.0))

            dis_bad_label_5 = get_target_tensor(dis_bad_5, False).to(device)
            dis_real_label_5 = get_target_tensor(dis_real_5, True).to(device)
            d_loss_5 = (adversarial_loss_5(dis_bad_5, dis_bad_label_5) + adversarial_loss_5(dis_real_5, dis_real_label_5)) * opt.lambda_d_adv


            dis_bad_6 = discriminator_6(resize_tensor(reconstructed_bad_rgb, 0.5))
            dis_real_6 = discriminator_6(resize_tensor(real_B_rgb, 0.5))

            dis_bad_label_6 = get_target_tensor(dis_bad_6, False).to(device)
            dis_real_label_6 = get_target_tensor(dis_real_6, True).to(device)
            d_loss_6 = (adversarial_loss_6(dis_bad_6, dis_bad_label_6) + adversarial_loss_6(dis_real_6, dis_real_label_6)) * opt.lambda_d_adv

            d_loss = d_loss_3 +\
                     d_loss_4 + \
                     d_loss_5 + \
                     d_loss_6

            d_loss.backward()
            optimizer_D.step()

        if i % 50 == 9:
            print(
                "[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [G loss: %f]"
                % (epoch, opt.n_epochs, i, len(dataloader), d_loss.item(), g_loss.item())
            )
    #save training images
    save_bad_quality = str(torch.norm(bad_mask, 2).item())
    save_image(real_A.data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_bad_quality}_real_bad.png'))
    save_image(torch.abs(bad_mask).data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_bad_quality}_bad_mask.png'))
    save_image(bad_content.data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_bad_quality}_bad_content.png'))
    save_image(reconstructed_bad_rgb.data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_bad_quality}_bad_recon.png'))

    save_good_quality = str(torch.norm(good_mask, 2).item())
    save_image(real_B.data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_good_quality}_real_good.png'))
    save_image(torch.abs(good_mask).data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_good_quality}_good_mask.png'))
    save_image(good_content.data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_good_quality}_good_content.png'))
    save_image(reconstructed_good_rgb.data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_good_quality}_good_recon.png'))
    #save model
    if epoch % 10 == 9:
        save_model(save_path, generator, epoch+1)
    #decay lr
    lr_scheduler_G.step()
    lr_scheduler_D.step()

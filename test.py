import argparse
import os
import numpy as np
import math
from PIL import Image

from sklearn import metrics
import matplotlib.pyplot as plt

import torchvision.transforms as transforms
from torchvision.utils import save_image

from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

from torch.optim import lr_scheduler
import torch.nn as nn
import torch.nn.functional as F
import torch

from Unet import UNet
from dataset import ImageDataset
import os
import kornia

parser = argparse.ArgumentParser()

parser.add_argument("--n_cpu", type=int, default=0, help="number of cpu threads to use during batch generation")
parser.add_argument("--dataroot", type=str, default='datasets/bad2good/', help='root directory of the dataset')
parser.add_argument("--save_name", type=str, default='bad2good', help='name of the model')
parser.add_argument("--epoch", type=int, default=550, help='number of epoch to load')
parser.add_argument("--hcut", type=float, default=0.45, help='Horizontal cutting portion')
parser.add_argument("--width", type=int, default=640, help='target image width')
parser.add_argument("--height", type=int, default=200, help='target image height')
parser.add_argument("--thresh", type=int, default=10.0, help='threshold value')


HSV = True

opt = parser.parse_args()
print(opt)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


#make test image save directory
model_path=os.path.join(opt.save_name, 'training')
save_path = os.path.join(opt.save_name, 'testing')
# save_path = 'no_norm'
if not os.path.exists(save_path):
    os.makedirs(save_path)

model_name = f'netG_{opt.epoch}.pth'
# model_name = 'nonorm.pth'

if model_name not in os.listdir(model_path):
    raise RuntimeError(f'model not founded in {model_path}')

G_path = os.path.join(model_path, model_name)

# Initialize generator
num_channel = 3
if HSV:
    num_channel = 6
generator = UNet(num_channel, bilinear=True)
generator = nn.DataParallel(generator)
generator.load_state_dict(torch.load(G_path))
generator.eval()

cuda = True if torch.cuda.is_available() else False
if cuda:
    generator.to(device)

#inference
ground_truth_list = []
prediction_list = []
path_A = os.listdir("datasets/bad2good/testA")
path_B = os.listdir("datasets/bad2good/testB")

path_A = [os.path.join("datasets/bad2good/testA",x) for x in path_A]
path_B = [os.path.join("datasets/bad2good/testB",x) for x in path_B]


#all
# img_list = path_A + path_B
# gt_list = [1 for _ in range(len(path_A))]+[0 for _ in range(len(path_B))]

#bad only
img_list = path_A
gt_list = [1 for _ in range(len(path_A))]

#good only
# img_list = path_B
# gt_list = [0 for _ in range(len(path_B))]

index = 0
for i, gt in zip(img_list, gt_list):
    index += 1
    # Set model input
    image = Image.open(i)
    image = image.resize((640,444))
    image_lower = image.crop((0, int(image.size[1] * opt.hcut), image.size[0], image.size[1]))
    image_upper = image.crop((0, 0, image.size[0], int(image.size[1] * opt.hcut)))
    # image_lower = image_lower.resize((opt.width, opt.height))
    # image_upper = image_upper.resize((opt.width, int(opt.height / opt.hcut) - opt.height))
    image_upper = ((np.array(image_upper) / 255.0))
    image_lower = ((np.array(image_lower) / 255.0))
    input_img = torch.tensor(image_lower).cuda().permute(2,0,1).unsqueeze(0).to(device, dtype=torch.float32)
    # input_img = torch.tensor(image_lower).cuda().unsqueeze(0).to(device, dtype=torch.float32)
    original_image = np.vstack((image_upper, image_lower))
    original_image = Image.fromarray(np.uint8((original_image*0.5+0.5)*255)).convert('RGB')

    if HSV:
        input_HSV = kornia.color.rgb_to_hsv(input_img)
        input_stack = torch.cat((input_img, input_HSV),dim=1)
    else:
        input_stack = input_img

    # Generate quality, mask and content
    mask, content = generator(input_stack)
    # mask_z = torch.zeros_like(mask).cuda()

    # mask = torch.where(mask > 0.3, mask, mask_z)

    # mask = torch.nn.functional.threshold(mask, 0.15, 0.02)

    # generate image
    # mask = quality * mask
    generated_img = (1.0 - mask) * input_img + mask * content
    content = (content + 1.0) / 2.0

    #save testing images
    pred_quality = torch.norm(mask, 2).item()
    ground_truth = gt
    save_image(input_img.data.cpu()[0], os.path.join(save_path, f'{index}_{str(pred_quality)}_{ground_truth}_input.png'))
    # original_image.save(os.path.join(save_path, i))
    save_image(mask.data.cpu()[0], os.path.join(save_path, f'{index}_{str(pred_quality)}_{ground_truth}_mask.png'))
    save_image(content.data.cpu()[0], os.path.join(save_path, f'{index}_{str(pred_quality)}_{ground_truth}_content.png'))
    save_image(generated_img.data.cpu()[0], os.path.join(save_path, f'{index}_{str(pred_quality)}_{ground_truth}_generated.png'))

    generated_lower = generated_img.cpu().detach().squeeze(0).numpy()
    generated_lower = np.transpose(generated_lower, (1, 2, 0))
    generated_image = np.vstack((image_upper, generated_lower))
    generated_image = Image.fromarray(np.uint8(generated_image * 255)).convert('RGB')
    generated_image.save(os.path.join(save_path, i.split('\\')[1].split('.')[0]+'.png'))

    prediction_list.append(pred_quality)
    ground_truth_list.append(int(ground_truth))

prediction_list = np.array(prediction_list)
ground_truth_list = np.array(ground_truth_list)
print(prediction_list)
prediction_list = np.where(prediction_list>opt.thresh, 1, 0)
print(prediction_list)
print(ground_truth_list)


accuracy = metrics.accuracy_score(ground_truth_list, prediction_list)
recall = metrics.recall_score(ground_truth_list, prediction_list, average="macro")
precision = metrics.precision_score(ground_truth_list, prediction_list, average="macro")
F1 = metrics.f1_score(ground_truth_list, prediction_list, average="macro")  
print("accuracy:",  accuracy, '\n', "precision:", precision, '\n', "recall:", recall, '\n', "F1 :",  F1)

display = metrics.PrecisionRecallDisplay.from_predictions(prediction_list, ground_truth_list)
_ = display.ax_.set_title("Precision-Recall Curve for extra data")
display.plot()
plt.show()

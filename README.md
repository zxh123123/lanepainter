This repo is the implementation of paper "LanePainter: Lane Marks Enhancement via Generative Adversarial Network"

### Requirement
---
1.Python >= 3.8

2.PyTorch >= 1.9

3.numpy

4.PIL

5.torchvision

6.kornia



### Training
```python trainHSVResize.py --dataroot YOUR_DATA_DIRECTORY --save_name YOUR_SAVE_NAME```


### Testing
```Python test.py --dataroot YOUR_DATA_DIRECTORY --save_name YOUR_MODEL_PATH --epoch YOUR_MODEL_EPOCHS```

Results will be saved under the model folder inside testing folder.


# Dataset

Please contact Xiaohan.Zhang@uvm.edu for dataset details


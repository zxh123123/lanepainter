""" Full assembly of the parts to form the complete network """
"""This code is modified from https://github.com/milesial/Pytorch-UNet"""

import torch
import torch.nn as nn
import torch.nn.functional as F

from unet_parts import *


class UNet(nn.Module):
    def __init__(self, in_channels, bilinear=False):
        super(UNet, self).__init__()
        self.in_channels = in_channels
        self.bilinear = bilinear

        self.inc = DoubleConv(in_channels, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        factor = 2 if bilinear else 1
        print(factor)
        self.down4 = Down(512, 1024 // factor)

        self.mask_up1 = Up(1024, 512 // factor, bilinear)
        self.mask_up2 = Up(512, 256 // factor, bilinear)
        self.mask_up3 = Up(256, 128 // factor, bilinear)
        self.mask_up4 = Up(128, 64, bilinear)

        self.content_up1 = Up(1024, 512 // factor, bilinear)
        self.content_up2 = Up(512, 256 // factor, bilinear)
        self.content_up3 = Up(256, 128 // factor, bilinear)
        self.content_up4 = Up(128, 64, bilinear)

        self.mask_conv = DoubleConv(64, 16)
        self.content_conv = DoubleConv(64, 16)

        self.mask_out_conv = nn.Conv2d(16, 1, kernel_size=1)
        self.content_out_conv = nn.Conv2d(16, 3, kernel_size=1)

        self.mask_activation = nn.Sigmoid()
        self.content_activation = nn.Tanh()
        # self.content_activation = nn.Sigmoid()

        # self.QualityEstimator = QualityEstimation()

    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        #encoding
        x5 = self.down4(x4)

        #up conv
        mask = self.mask_up1(x5, x4)
        mask = self.mask_up2(mask, x3)
        mask = self.mask_up3(mask, x2)
        mask = self.mask_up4(mask, x1)

        content = self.content_up1(x5, x4)
        content = self.content_up2(content, x3)
        content = self.content_up3(content, x2)
        content = self.content_up4(content, x1)
        #generate mask and content
        mask = self.mask_conv(mask)
        content = self.content_conv(content)

        mask = self.mask_out_conv(mask)
        content = self.content_out_conv(content)

        mask = self.mask_activation(mask)
        content = self.content_activation(content)

        return mask, content

if __name__ == "__main__":
    test_net = UNet(3, bilinear=True)
    input_tensor = torch.rand((1,3,245,640))
    mask, content = test_net.forward(input_tensor)
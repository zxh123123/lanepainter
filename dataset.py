"""Adopted from https://github.com/sarathknv/AGGAN/blob/master/datasets.py"""
import glob
import random
import os

from torch.utils.data import Dataset
from PIL import Image
import torchvision.transforms as transforms

class ImageDataset(Dataset):
    def __init__(self, root, transforms_=None, unaligned=False, mode='train'):
        self.transform = transforms.Compose(transforms_)
        self.unaligned = unaligned
        self.mode = mode

        if self.mode == 'train':
            self.files_A = sorted(glob.glob(os.path.join(root, 'trainA') + '/*.*'))
            self.files_B = sorted(glob.glob(os.path.join(root, 'trainB') + '/*.*'))
        elif self.mode == "val":
            self.files_A = sorted(glob.glob(os.path.join(root, 'valA') + '/*.*'))
            self.files_B = sorted(glob.glob(os.path.join(root, 'valB') + '/*.*'))
        elif self.mode == "test":
            self.files_A = sorted(glob.glob(os.path.join(root, 'testA') + '/*.*'))
            self.files_B = sorted(glob.glob(os.path.join(root, 'testB') + '/*.*'))
        else:
            raise RuntimeError("no such mode")

    def __getitem__(self, index):
        if self.mode == 'train':
            item_A = self.transform(Image.open(self.files_A[index % len(self.files_A)]))

            if self.unaligned:
                item_B = self.transform(Image.open(self.files_B[random.randint(0, len(self.files_B) - 1)]))
            else:
                item_B = self.transform(Image.open(self.files_B[index % len(self.files_B)]))

            return {'A': item_A, 'B': item_B}
        elif self.mode == 'val' or self.mode == 'test':
            if index < len(self.files_A):
                item = self.transform(Image.open(self.files_A[index]))
                label = 1.0
            if index >= len(self.files_A):
                item = self.transform(Image.open(self.files_B[index - len(self.files_A)]))
                label = 0.0
            return {'item':item, 'label': label}


    def __len__(self):
        if self.mode == 'train':
            return max(len(self.files_A), len(self.files_B))
        elif self.mode == 'val' or self.mode == 'test':
            return len(self.files_A) + len(self.files_B)
        else:
            raise RuntimeError("no such mode")

# if __name__ == "__main__":
#     # Configure data loader
#     transforms_ = [ transforms.RandomHorizontalFlip(),
#         transforms.Lambda(lambda img: img.crop((0, int(img.size[1] * 0.45), img.size[0], img.size[1]))),
#         transforms.Resize((200 + 15, 640 + 25)),
#         transforms.RandomCrop((200, 640)),
#         transforms.RandomAffine(degrees=8, translate=(0.1, 0.1), scale=(0.9, 1), shear=(6, 6)),
#         transforms.ColorJitter(0.2, 0.2, 0.2),
#         transforms.RandomGrayscale(),
#         transforms.ToTensor(),
#         transforms.RandomVerticalFlip(p=0.2),
#         transforms.RandomErasing(p=0.2,scale=(0.02, 0.1), ratio=(0.6, 1.5)),
#         transforms.Normalize([0.5,0.5,0.5], [0.5,0.5,0.5]) 
#         ]

#     dataloader = DataLoader(ImageDataset("datasets/bad2good", transforms_=transforms_, unaligned=True), 
#                             batch_size=3, shuffle=True, num_workers=1)

#     for i,b in enumerate(dataloader):
#         print(b['A'].shape)
#         break
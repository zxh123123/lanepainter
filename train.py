import argparse
import os
import numpy as np
import math
import itertools

import torchvision.transforms as transforms
from torchvision.utils import save_image

from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

from torch.optim import lr_scheduler
import torch.nn as nn
import torch.nn.functional as F
import torch

from Unet import UNet
from discriminator import discriminator
from dataset import ImageDataset
from gradient_penalty import gradient_penalty

parser = argparse.ArgumentParser()
parser.add_argument("--n_epochs", type=int, default=2000, help="number of epochs of training")
parser.add_argument("--epoch", type=int, default=0, help="number of epochs start training")
parser.add_argument("--decay_epoch", type=int, default=100, help="number of epochs start decaying LR")
parser.add_argument("--batch_size", type=int, default=36, help="size of the batches")
parser.add_argument("--lr", type=float, default=1e-5, help="learning rate")
parser.add_argument("--lambda_similarity", type=float, default=1e-3, help="similarity loss weight")
parser.add_argument("--lambda_mask", type=float, default=1e-4, help="mask loss weight")
parser.add_argument("--lambda_gan", type=float, default=10.0, help="adversarial loss weight")
parser.add_argument("--lambda_smooth", type=float, default=1e-6, help="mask smooth loss")
parser.add_argument("--color_smooth", type=float, default=1e-6, help="color smooth loss")
parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
parser.add_argument("--dataroot", type=str, default='datasets/bad2good/', help='root directory of the dataset')
parser.add_argument("--width", type=int, default=640, help='target image width')
parser.add_argument("--height", type=int, default=200, help='target image height')
parser.add_argument("--save_name", type=str, default='testWGAN', help='name of the model')
parser.add_argument("--beta1", type=float, default=0.5, help='beta1 for adam')
parser.add_argument("--beta2", type=float, default=0.999, help='beta2 for adam')
parser.add_argument("--hcut", type=float, default=0.45, help='Horizontal cutting portion')
parser.add_argument("--n_critic", type=int, default=2, help="number of training steps for discriminator per iter")
parser.add_argument("--good_penalty", type=float, default=100.0, help="good mask penalty term")
parser.add_argument("--mask_clip", type=bool, default=False, help="toggle flag to enable mask clip")
parser.add_argument("--mask_clip_value", type=float, default=0.3, help="mask clip value")

opt = parser.parse_args()
print(opt)

class LambdaLR():
    def __init__(self, n_epochs, offset, decay_start_epoch):
        '''
        n_epochs: number of total training epochs
        offset: train start epochs
        decay_start_epoch: epoch start decay
        '''
        assert ((n_epochs - decay_start_epoch) > 0), "Decay must start before the training session ends!"
        self.n_epochs = n_epochs
        self.offset = offset
        self.decay_start_epoch = decay_start_epoch

    def step(self, epoch):
        return 1.0 - max(0, epoch + self.offset - self.decay_start_epoch)/(self.n_epochs - self.decay_start_epoch)

def save_model(name, generator, discriminator_orig, discriminator_smaller2, discriminator_enlarge2, epoch):
    torch.save(generator.state_dict(), os.path.join(name, f'netG_{epoch}.pth'))



def get_target_tensor(prediction, target_is_real):
        """Create label tensors with the same size as the input.
        Parameters:
        """
        if target_is_real:
            target_tensor =  torch.tensor(1.0) # real label
        else:
            target_tensor = torch.tensor(0.0) # fake label
        return target_tensor.expand_as(prediction)

def smooth_loss(mat):
    '''
    compute smooth loss for mask.
    '''
    return torch.sum(torch.abs(mat[:, :, :, :-1] - mat[:, :, :, 1:])) + \
            torch.sum(torch.abs(mat[:, :, :-1, :] - mat[:, :, 1:, :]))

cuda = True if torch.cuda.is_available() else False

# Loss function
# adversarial_loss_orig = nn.BCEWithLogitsLoss()
# adversarial_loss_smaller2 = nn.BCEWithLogitsLoss()
# adversarial_loss_enlarge4 = nn.BCEWithLogitsLoss()
# adversarial_loss_enlarge2 = nn.BCEWithLogitsLoss() #enlarge
quality_loss = nn.BCEWithLogitsLoss()
similarity_loss = nn.SmoothL1Loss()

# Initialize generator and discriminator
generator = UNet(3, bilinear=True)
discriminator_orig = discriminator(3,64,3)
discriminator_smaller2 = discriminator(3,64,4)
discriminator_enlarge4 = discriminator(3,64,5)
discriminator_enlarge2 = discriminator(3,64,6)

if torch.cuda.device_count() > 1:
  generator = nn.DataParallel(generator)
  discriminator_orig = nn.DataParallel(discriminator_orig)
  discriminator_smaller2 = nn.DataParallel(discriminator_smaller2)
  discriminator_enlarge4 = nn.DataParallel(discriminator_enlarge4)
  discriminator_enlarge2 = nn.DataParallel(discriminator_enlarge2)

if cuda:
    generator.cuda()
    discriminator_orig.cuda()
    discriminator_smaller2.cuda()
    discriminator_enlarge4.cuda()
    discriminator_enlarge2.cuda()
    # adversarial_loss_orig.cuda()
    # adversarial_loss_smaller2.cuda()
    # adversarial_loss_enlarge4.cuda()
    # adversarial_loss_enlarge2.cuda()
    quality_loss.cuda()
    similarity_loss.cuda()

# Configure data loader
transforms_ = [ transforms.RandomHorizontalFlip(),
                transforms.Lambda(lambda img: img.crop((0, int(img.size[1] * opt.hcut), img.size[0], img.size[1]))),
                transforms.Resize((opt.height + 15, opt.width + 25)),
                transforms.RandomCrop((opt.height, opt.width)),
                transforms.RandomAffine(degrees=8, translate=(0.1, 0.1), scale=(0.9, 1), shear=(6, 6)),
                transforms.ColorJitter(0.2, 0.2, 0.2),
                transforms.RandomGrayscale(),
                transforms.ToTensor(),
                transforms.RandomVerticalFlip(p=0.2),
                transforms.RandomErasing(p=0.2,scale=(0.02, 0.1), ratio=(0.6, 1.5)),
                transforms.Normalize([0.5,0.5,0.5], [0.5,0.5,0.5]) 
                ]

dataloader = DataLoader(ImageDataset(opt.dataroot, transforms_=transforms_, unaligned=True), 
                        batch_size=opt.batch_size, shuffle=True, num_workers=opt.n_cpu)

# Optimizers
optimizer_G = torch.optim.Adam(generator.parameters(), lr=opt.lr, betas=(opt.beta1, opt.beta2), weight_decay=3e-6)
optimizer_D = torch.optim.Adam(itertools.chain(discriminator_orig.parameters(), discriminator_smaller2.parameters(),\
     discriminator_enlarge2.parameters(), discriminator_enlarge4.parameters()), lr=opt.lr, betas=(opt.beta1, opt.beta2), weight_decay=1e-6)

#learning rate scheduler
lr_scheduler_G = torch.optim.lr_scheduler.LambdaLR(optimizer_G, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)
lr_scheduler_D = torch.optim.lr_scheduler.LambdaLR(optimizer_D, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)

Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor

#check saving path exists before training
save_path='%s/%s' % (opt.save_name, 'training')
save_image_path = os.path.join(save_path, 'training_samples')
if not os.path.exists(save_path):
    os.makedirs(save_path)
if not os.path.exists(save_image_path):
    os.makedirs(save_image_path)

# ----------
#  Training
# ----------


for epoch in range(opt.n_epochs):
    for i, batch in enumerate(dataloader):
        # Set model input
        input_A = Tensor(batch['A'].shape[0], 3, batch['A'].shape[2], batch['A'].shape[3])
        input_B = Tensor(batch['B'].shape[0], 3, batch['B'].shape[2], batch['B'].shape[3])
        real_A = Variable(input_A.copy_(batch['A']))
        real_B = Variable(input_B.copy_(batch['B']))


        # ---------------------
        #  Train Discriminator
        # ---------------------
        # Generate bad quality, mask and content
        bad_mask, bad_content = generator(real_A)


        if opt.mask_clip:#if need clip here
            # mask_thresh = nn.Threshold(opt.mask_clip_value, 0)
            bad_thresh_mask = torch.nn.functional.threshold(bad_mask, opt.mask_clip_value, 0)
            reconstructed_bad = (1.0 - bad_thresh_mask) * real_A + bad_thresh_mask * bad_content

        # reconstructing image
        else:
            reconstructed_bad = (1.0 - bad_mask) * real_A + bad_mask * bad_content

        #detach reconstructed image for discriminator
        reconstructed_bad = reconstructed_bad.detach()

        optimizer_D.zero_grad()

        def discriminatorLoss(reconstructed_bad, real_B, critic):
            # generate discriminator result
            dis_bad = critic(reconstructed_bad)
            dis_real = critic(real_B)

            gp_loss = gradient_penalty(real_B, reconstructed_bad, critic)
            single_d_loss = dis_bad.mean() - dis_real.mean() + opt.lambda_gan * gp_loss
            return single_d_loss
        
        d_loss_orig = discriminatorLoss(reconstructed_bad, real_B, discriminator_orig)
        d_loss_enlarge2 = discriminatorLoss(reconstructed_bad, real_B, discriminator_enlarge2)
        d_loss_enlarge4 = discriminatorLoss(reconstructed_bad, real_B, discriminator_enlarge4)
        d_loss_smaller2 = discriminatorLoss(reconstructed_bad, real_B, discriminator_smaller2)


        d_loss = d_loss_orig + \
                d_loss_smaller2 + \
                d_loss_enlarge2 + \
                d_loss_enlarge4


        d_loss.backward()
        optimizer_D.step()


        # -----------------
        #  Train Generator
        # -----------------
        critic_number = opt.n_critic
        if i % critic_number == 0:

            optimizer_G.zero_grad()

            # Generate good and bad quality, mask and content
            bad_mask, bad_content = generator(real_A)
            good_mask, good_content = generator(real_B)

            if opt.mask_clip:#if need clip here
                bad_thresh_mask = torch.nn.functional.threshold(bad_mask, opt.mask_clip_value, 0)
                reconstructed_bad = (1.0 - bad_thresh_mask) * real_A + bad_thresh_mask * bad_content

                good_thresh_mask = torch.nn.functional.threshold(good_mask, opt.mask_clip_value, 0)
                reconstructed_good = (1.0 - good_thresh_mask) * real_A + good_thresh_mask * good_content

            # reconstructing image
            else:
                reconstructed_bad = (1.0 - bad_mask) * real_A + bad_mask * bad_content
                reconstructed_good = (1.0 - good_mask) * real_B + good_mask * good_content

            def GeneratorLoss(reconstructed_bad, critic):
                d_bad = critic(reconstructed_bad)
                g_adversarial_loss = - d_bad.mean()
                return g_adversarial_loss

            #calculate loss for generator
            color_smooth_loss = smooth_loss(bad_content)
            mask_smooth_loss = smooth_loss(bad_mask)
            mask_norm_loss = torch.norm(bad_mask, 2) + torch.norm(good_mask, 2) * opt.good_penalty
            total_similarity_loss = similarity_loss(real_A, reconstructed_bad) + similarity_loss(real_B, reconstructed_good)

            g_adversarial_loss_orig = GeneratorLoss(reconstructed_bad, discriminator_orig)
            g_adversarial_loss_enlarge2 = GeneratorLoss(reconstructed_bad, discriminator_enlarge2)
            g_adversarial_loss_enlarge4 = GeneratorLoss(reconstructed_bad, discriminator_enlarge4)
            g_adversarial_loss_smaller2 = GeneratorLoss(reconstructed_bad, discriminator_smaller2)
            g_adversarial_loss = g_adversarial_loss_orig + g_adversarial_loss_smaller2 +\
                g_adversarial_loss_enlarge2 + g_adversarial_loss_enlarge4

            g_loss = g_adversarial_loss + \
                    opt.lambda_similarity * total_similarity_loss + \
                    opt.lambda_mask * mask_norm_loss + \
                    opt.lambda_smooth * mask_smooth_loss + \
                    opt.color_smooth * color_smooth_loss

            g_loss.backward()
            optimizer_G.step()

        if i % 10 == 0:
            print(
                "[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [G loss: %f]"
                % (epoch, opt.n_epochs, i, len(dataloader), d_loss.item(), g_loss.item())
            )
    #save training images
    save_bad_quality = str(torch.norm(bad_mask, 2).item())
    save_image(real_A.data.cpu()[0] * 0.5 + 0.5, os.path.join(save_image_path, f'{epoch}_{save_bad_quality}_real_bad.png'))
    save_image(torch.abs(bad_mask).data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_bad_quality}_bad_mask.png'))
    save_image(bad_content.data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_bad_quality}_bad_content.png'))
    save_image(reconstructed_bad.data.cpu()[0] * 0.5 + 0.5, os.path.join(save_image_path, f'{epoch}_{save_bad_quality}_bad_recon.png'))

    save_good_quality = str(torch.norm(good_mask, 2).item())
    save_image(real_B.data.cpu()[0] * 0.5 + 0.5, os.path.join(save_image_path, f'{epoch}_{save_good_quality}_real_good.png'))
    save_image(torch.abs(good_mask).data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_good_quality}_good_mask.png'))
    save_image(good_content.data.cpu()[0], os.path.join(save_image_path, f'{epoch}_{save_good_quality}_good_content.png'))
    save_image(reconstructed_good.data.cpu()[0] * 0.5 + 0.5, os.path.join(save_image_path, f'{epoch}_{save_good_quality}_good_recon.png'))
    #save model
    if epoch % 50 == 9:
        save_model(save_path, generator, discriminator_orig, discriminator_smaller2, discriminator_enlarge2, epoch+1)
    #decay lr
    lr_scheduler_G.step()
    lr_scheduler_D.step()
